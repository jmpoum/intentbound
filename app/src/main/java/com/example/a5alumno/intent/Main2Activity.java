package com.example.a5alumno.intent;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Bundle b =getIntent().getExtras();
        String param1 = b.getString(("param1"));
        String param2 = b.getString(("param2"));
        String param3 = b.getString(("param3"));
        Toast.makeText(getApplicationContext(),param1 + " + " + param2 + " + " + param3, Toast.LENGTH_LONG).show();


        final Button button = (Button) findViewById(R.id.btornar);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Perform action on click
                Intent x = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(x);
                finish();;

            }
        });

    }
}
